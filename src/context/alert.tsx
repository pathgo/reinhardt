import React, { createContext } from "react";
import Alert, { AlertType, useAlertState } from "../components/alert";

export type AlertContextQueueAlertFN = (message: string) => void;

type AlertContextValue = {
  queueErrorAlert: AlertContextQueueAlertFN;
};

const unimplemented = () => {
  throw new Error("User context provider has not been yet initialized");
};

const AlertContext = createContext<AlertContextValue>({
  queueErrorAlert: unimplemented,
});

export const AlertQueueContextProvider: React.FC = ({ children }) => {
  const {
    isOpen: alertOpen,
    openAlert,
    closeAlert,
    alertMessage,
    alertType,
  } = useAlertState();

  const queueErrorAlert = (message: string) => {
    openAlert(message, AlertType.Error);
  };

  return (
    <AlertContext.Provider value={{ queueErrorAlert }}>
      <Alert
        autohide
        isOpen={alertOpen}
        type={alertType}
        closeAlert={closeAlert}
      >
        {alertMessage}
      </Alert>
      {children}
    </AlertContext.Provider>
  );
};

export default AlertContext;

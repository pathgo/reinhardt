import React, { createContext, useState } from "react";
import { IUser } from "../service/auth";

export type UserContextValue = {
  user?: IUser;
  setUser: (user: IUser) => void;
  clearUser: () => void;
};

const unimplemented = () => {
  throw new Error("User context provider has not been yet initialized");
};

const UserContext = createContext<UserContextValue>({
  setUser: unimplemented,
  clearUser: unimplemented,
});

export const UserContextProvider: React.FC = ({ children }) => {
  const [user, setUserState] = useState<IUser>();

  const clearUser = () => {
    setUserState(undefined);
  };

  const setUser = (newUser: IUser) => setUserState(newUser);

  return (
    <UserContext.Provider value={{ user, setUser, clearUser }}>
      {children}
    </UserContext.Provider>
  );
};

export default UserContext;

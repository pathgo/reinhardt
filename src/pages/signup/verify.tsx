import { makeStyles } from "@material-ui/core";
import React from "react";
import { useHistory } from "react-router-dom";
import Button from "../../components/button";
import NavBar, { NavbarNavItem } from "../../components/navbar";

const useHomepageStyles = makeStyles({
  wrapper: {
    marginLeft: "auto",
    marginRight: "auto",
    width: "40vw",
  },
  loginButton: {
    textAlign: "center",
  },
});

const NavbarNav: Array<NavbarNavItem> = [
  {
    title: "Home",
    link: "/",
  },
];

const VerifyPage: React.FC = () => {
  const classes = useHomepageStyles();
  const history = useHistory();

  return (
    <div>
      <NavBar title="PathGo" navItems={NavbarNav} />
      <div className={classes.wrapper}>
        <h2>Thank you for choosing PathGo! </h2>
        <p>
          Please verify your email address by clicking on the link sent to you!
        </p>
        <Button
          onClick={() => history.push("/auth/login")}
          className={classes.loginButton}
        >
          Login
        </Button>
      </div>
    </div>
  );
};
export default VerifyPage;

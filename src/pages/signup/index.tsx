import React, { useState } from "react";
import { makeStyles } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import useFormChange from "../../components/form/textfield/useFormChange";
import TextFieldComponent from "../../components/form/textfield";
import AuthLayout from "../../components/layout/auth";
import AuthService from "../../service/auth";

const useStyles = makeStyles({
  textField: {
    marginBottom: "12px",
  },
});
const SignUpPage: React.FC = () => {
  const classes = useStyles();
  // ========= Form Inputs =========

  const [email, setemail] = useFormChange("");
  const [password, setpassword] = useFormChange("");

  const [errorMessage, setErrorMessage] = useState<string>();
  const history = useHistory();

  const handleSignUp = async () => {
    if (errorMessage) setErrorMessage(undefined);
    await AuthService.Signup({ email, password });
    history.push(`/auth/verify`);
  };

  return (
    <AuthLayout
      title="Get Started!"
      buttonTitle="Signup"
      onSubmit={handleSignUp}
    >
      <TextFieldComponent
        className={classes.textField}
        required
        fullWidth
        autoComplete="email"
        type="email"
        label="Email"
        placeholder="Email"
        value={email}
        onChange={setemail}
      />
      <TextFieldComponent
        required
        fullWidth
        type="password"
        label="Password"
        autoComplete="password"
        placeholder="Password"
        value={password}
        onChange={setpassword}
      />
    </AuthLayout>
  );
};
export default SignUpPage;

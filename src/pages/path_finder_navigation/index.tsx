import { Divider } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import FloorPlanGrid, { GridNodeProps } from "../../components/floor_plan_grid";
import SimpleNavbarLayout from "../../components/layout/simple/navbar";
import LinearProgress from "../../components/linear_progress";
import BusinessService from "../../service/api/business";
import FloorPlanService from "../../service/api/floorplan";
import { LocationGridNodeProps } from "../../service/api/types";
import useQueryParams from "../../utils/router/use_query_params";

export type NavigationScreenQueryParams = {
  startRow: string;
  startCol: string;
  endRow: string;
  endCol: string;
};

const PathFinderNavigationScreen: React.FC = () => {
  const { businessId } = useParams<{
    businessId: string;
  }>();
  const {
    startRow,
    startCol,
    endRow,
    endCol,
  } = useQueryParams<NavigationScreenQueryParams>();
  const [gridNodes, setGridNodes] = useState<Array<Array<GridNodeProps>>>([]);
  const [gridLoading, setGridLoading] = useState(false);
  const [navPathNodes, setNavPathNodes] = useState<
    Array<LocationGridNodeProps>
  >();

  useEffect(() => {
    const populateGridNodes = async () => {
      const business = await BusinessService.FindById(businessId);
      const { floorGrid } = business.floorPlan[0];
      const newGridNode = floorGrid.map((row) =>
        row.map((node) => ({
          nodeType: node.nodeType,
          highlight: false,
          nodeProps: node.nodeProps,
        }))
      );
      setGridNodes(newGridNode);
    };

    const populatePathNodes = async () => {
      if (!!startRow || !!startCol || !!endRow || !!endCol) {
        // TODO: Show Error
      }

      const pathNodeResult = await FloorPlanService.GetDestinationPath(
        businessId,
        {
          row: parseInt(startRow as string, 10),
          col: parseInt(startCol as string, 10),
        },
        {
          row: parseInt(endRow as string, 10),
          col: parseInt(endCol as string, 10),
        }
      );
      setNavPathNodes(pathNodeResult);
    };

    const runSetup = async () => {
      try {
        setGridLoading(true);
        await Promise.all([populateGridNodes(), populatePathNodes()]);
      } catch (error) {
        // eslint-disable-next-line no-console
        console.log(error);
      } finally {
        setGridLoading(false);
      }
    };

    runSetup();
  }, [businessId, endCol, endRow, startCol, startRow]);

  return (
    <SimpleNavbarLayout>
      {gridLoading && <LinearProgress />}
      <Divider />
      <FloorPlanGrid
        gridNodes={gridNodes}
        editable={false}
        userLocation={{
          row: parseInt(startRow as string, 10),
          col: parseInt(startCol as string, 10),
        }}
        navPathNodes={navPathNodes}
      />
    </SimpleNavbarLayout>
  );
};

export default PathFinderNavigationScreen;

import { createStyles, makeStyles } from "@material-ui/core";
import React, { useEffect, useMemo, useState } from "react";
import DashBusinessItemList from "../../components/business_list_item/dashboard/DashBusinessItemList";
import LinearProgress from "../../components/linear_progress";
import Text from "../../components/Text";
import { ThemeColor } from "../../constant/theme";
import BusinessService from "../../service/api/business";
import { BusinessModelSummary } from "../../service/api/types";
import BusinessDashboardNavBar from "./navbar";

const useStyles = makeStyles((theme: ThemeColor) =>
  createStyles({
    root: {
      flexGrow: 1,
      backgroundColor: theme,
      marginTop: "24px",
      marginLeft: "50px",
      marginRight: "50px",
    },
    header: {
      textAlign: "left",
    },
  })
);

const BusinessDashboardPage: React.FC = () => {
  const [userBusinesses, setUserBusinesses] = useState<
    Array<BusinessModelSummary>
  >([]);
  const [loading, setLoading] = useState(true);
  const [errorMessage, setErrorMessage] = useState<string>();

  useEffect(() => {
    const fetchBusinesses = async () => {
      try {
        setLoading(true);
        const businsses = await BusinessService.GetCurrentUserBusinesses();
        setUserBusinesses(businsses);
      } catch (error) {
        setErrorMessage("Unable to fetch user businesses");
      } finally {
        setLoading(false);
      }
    };

    fetchBusinesses();
  }, []);

  const classes = useStyles();

  const removeUserBusinessFromState = (businessId: string) => {
    const newBusinessList = userBusinesses.filter(
      (entry) => entry.id !== businessId
    );
    setUserBusinesses(newBusinessList);
  };

  const businessListView = useMemo(
    () => (
      <DashBusinessItemList
        results={userBusinesses}
        onDelete={removeUserBusinessFromState}
      />
    ),
    [userBusinesses]
  );

  return (
    <div className={classes.header}>
      <BusinessDashboardNavBar title="Welcome Back!" />
      {loading && <LinearProgress />}
      <div className={classes.root}>
        {errorMessage && <Text color={ThemeColor.Danger}>{errorMessage}</Text>}
        {businessListView}
      </div>
    </div>
  );
};

export default BusinessDashboardPage;

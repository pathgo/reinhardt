import { makeStyles } from "@material-ui/core";
import React, { useMemo } from "react";
import { useHistory } from "react-router-dom";
import Button from "../../../components/button";
import NavBar from "../../../components/navbar";
import { ThemeColor } from "../../../constant/theme";

type BusinessDashboardNavBarProps = {
  title: string;
};

const useStyles = makeStyles({
  homeButton: {
    marginRight: "20px",
  },
  createButton: {
    marginRight: "20px",
  },
});

const BusinessDashboardNavBar: React.FC<BusinessDashboardNavBarProps> = ({
  title,
}) => {
  const history = useHistory();
  const classes = useStyles();
  const rightContent = useMemo(
    () => (
      <>
        <Button
          className={classes.homeButton}
          color={ThemeColor.Light}
          onClick={() => history.push("/")}
        >
          Home
        </Button>
        <Button
          className={classes.createButton}
          color={ThemeColor.Secondary}
          onClick={() => history.push("/floorplan/create")}
        >
          Create
        </Button>
      </>
    ),
    [classes.createButton, classes.homeButton, history]
  );
  return <NavBar title={title} right={rightContent} />;
};

export default BusinessDashboardNavBar;

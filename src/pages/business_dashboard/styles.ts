import { makeStyles } from "@material-ui/core";
import { ThemeColor } from "../../constant/theme";

export default makeStyles({
  wrapper: {
    // height: "100vh",
    margin: 0,
    padding: 0,
  },
  header: {
    position: "fixed",
    top: 0,
    zIndex: 2,
    width: "100%",
  },
  content: {
    margin: 0,
    padding: 0,
    background: ThemeColor.White,
  },
});

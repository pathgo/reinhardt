import { makeStyles } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import FloorPlanGrid, {
  GridNodeProps,
} from "../../../components/floor_plan_grid";
import LinearProgress from "../../../components/linear_progress";
import BusinessService from "../../../service/api/business";

const useStyles = makeStyles({
  root: {
    marginLeft: "auto",
    marginRight: "auto",
  },
  title: {
    textAlign: "center",
  },
  nextPageBTN: {
    display: "flex",
    justifyContent: "flex-end",
  },
});

export type CurrentLocationPromptProps = {
  businessId: string;
  onLocationSelect: (row: number, col: number) => void;
};

const CurrentLocationPrompt: React.FC<CurrentLocationPromptProps> = ({
  businessId,
  onLocationSelect,
}) => {
  const classes = useStyles();
  const [floorGrid, setFloorGrid] = useState<Array<Array<GridNodeProps>>>([]);
  const [loadingGrid, setLoadingGrid] = useState(false);

  useEffect(() => {
    const updateFloorGrid = async () => {
      setLoadingGrid(true);
      try {
        const business = await BusinessService.FindById(businessId);
        const { floorGrid: floorGridResponse } = business.floorPlan[0];
        const newGridNode = floorGridResponse.map((row) =>
          row.map((node) => {
            const currentNode: GridNodeProps = {
              nodeType: node.nodeType,
              highlight: false,
              nodeProps: node.nodeProps,
            };
            return currentNode;
          })
        );
        setFloorGrid(newGridNode);
      } finally {
        setLoadingGrid(false);
      }
    };
    updateFloorGrid();
  }, [businessId]);

  const onSelectLocation = (row: number, col: number) => {
    onLocationSelect(row, col);
  };

  return (
    <div className={classes.root}>
      <h1 className={classes.title}>Select your location</h1>
      {loadingGrid && <LinearProgress />}
      <FloorPlanGrid
        gridNodes={floorGrid}
        editable={false}
        onNodeSelected={onSelectLocation}
      />
    </div>
  );
};

export default CurrentLocationPrompt;

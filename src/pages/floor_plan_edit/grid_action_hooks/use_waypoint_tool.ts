import { cloneDeep } from "lodash";
import { useEffect, useState } from "react";
import { FloorGridNodeType } from "../../../service/api/types";
import { UseGridWaypointActionHook } from "./type";

const useWaypointTool: UseGridWaypointActionHook = (
  gridNodes,
  setGridNodes
) => {
  const [existingWaypoints, setExistingWaypoints] = useState<Set<string>>(
    new Set()
  );

  useEffect(() => {
    const waypoints = new Set<string>();
    gridNodes.forEach((row) => {
      row.forEach((node) => {
        if (node.nodeProps?.title) waypoints.add(node.nodeProps.title);
      });
    });
    setExistingWaypoints(waypoints);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [gridNodes.length]);

  const setWaypoint = (row: number, col: number, title: string) => {
    if (title.length === 0) throw new Error("Title cannot be empty");
    if (existingWaypoints.has(title)) throw new Error("Title already exists");
    const newGridNodes = cloneDeep(gridNodes);
    const selectedNode = newGridNodes[row][col];
    selectedNode.nodeType = FloorGridNodeType.Waypoint;
    selectedNode.nodeProps = {
      title,
    };
    setGridNodes(newGridNodes);
    const newWaypointSet = Array.from(existingWaypoints);
    newWaypointSet.push(title);
    setExistingWaypoints(new Set(newWaypointSet));
  };

  const removeWaypoint = (row: number, col: number) => {
    const newGridNodes = cloneDeep(gridNodes);
    const selectedNode = newGridNodes[row][col];
    selectedNode.nodeType = FloorGridNodeType.Path;
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const { title } = selectedNode.nodeProps!;
    const newWaypointSet = new Set(existingWaypoints);
    newWaypointSet.delete(title);
    setExistingWaypoints(newWaypointSet);
    delete selectedNode.nodeProps;
    setGridNodes(newGridNodes);
  };

  return {
    setWaypoint,
    removeWaypoint,
  };
};
export default useWaypointTool;

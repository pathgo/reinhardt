import React, { useEffect, useMemo, useState } from "react";
import { RouteComponentProps } from "react-router-dom";
import FloorPlanNavBar from "./navbar";
import FloorPlanToolBar, { ToolbarToolType } from "./toolbar";
import useStyles from "./styles";
import FloorPlanGrid, { GridNodeProps } from "../../components/floor_plan_grid";
import useLineTool from "./grid_action_hooks/use_line_tool";
import useRectangleTool from "./grid_action_hooks/use_rectangle_tool";
import useWaypointTool from "./grid_action_hooks/use_waypoint_tool";
import Alert, { AlertType, useAlertState } from "../../components/alert";
import LinearProgress from "../../components/linear_progress";
import BusinessService from "../../service/api/business";
import FloorPlanService from "../../service/api/floorplan";
import { FloorGridNodeType, FloorPlanGridNode } from "../../service/api/types";
import { PromptWaypointOptions } from "./prompt_waypoint";
import useWaypointPromptModalState from "./prompt_waypoint/use_waypoint_prompt_state";

// Controller Class
const FloorPlanEditPage: React.FC<
  RouteComponentProps<{ businessId: string }>
> = ({
  match: {
    params: { businessId },
  },
}) => {
  // ========== States ==========
  const classes = useStyles();
  const [selectedTool, setSelectedTool] = useState(ToolbarToolType.Line);
  const [businessTitle, setBusinessTitle] = useState("");
  const [loadingGrid, setLoadingGrid] = useState(false);
  const [gridNodes, setGridNodes] = useState<Array<Array<GridNodeProps>>>([]);
  const [currentFloor, setCurrentFloor] = useState("");
  const {
    isOpen: isAlertOpen,
    openAlert,
    closeAlert,
    alertMessage,
    alertType,
  } = useAlertState();

  // ========== Tool Action Hooks ==========
  const {
    nodeSelected: lineNodeSelected,
    deselectTool: deselectLineTool,
  } = useLineTool(gridNodes, setGridNodes);
  const {
    nodeSelected: rectangleNodeSelect,
    deselectTool: deselectRectangleTool,
  } = useRectangleTool(gridNodes, setGridNodes);
  const waypointTool = useWaypointTool(gridNodes, setGridNodes);
  const {
    displayPrompt,
    waypointPromptCurrentTitle,
    openWaypointPrompt,
    onSaveWaypoint,
    onCancelCreateWaypoint,
    onDeleteWaypointCurrentWaypoint,
  } = useWaypointPromptModalState(waypointTool);

  const [disableSelector] = useState(
    new Set([ToolbarToolType.Pointer, ToolbarToolType.UserLocation])
  );

  useEffect(() => {
    const fetchFloorPlanInfo = async () => {
      try {
        setLoadingGrid(true);
        const business = await BusinessService.FindById(businessId);
        const { floorTitle, floorGrid } = business.floorPlan[0];
        const newGridNode = floorGrid.map((row) =>
          row.map((node) => {
            const currentNode: GridNodeProps = {
              nodeType: node.nodeType,
              highlight: false,
              nodeProps: node.nodeProps,
            };
            return currentNode;
          })
        );
        setCurrentFloor(floorTitle);
        setGridNodes(newGridNode);
        setBusinessTitle(business.businessName);
        setLoadingGrid(false);
      } catch (error) {
        setLoadingGrid(false);
      }
    };

    fetchFloorPlanInfo();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [businessId]);

  // ========== JSX Elements ==========
  const navbarView = useMemo(() => {
    const handleFloorPlanSave = async () => {
      try {
        setLoadingGrid(true);
        await FloorPlanService.SetFloorplanForBusiness(businessId, [
          {
            floorTitle: currentFloor,
            floorGrid: gridNodes.map((row) =>
              row.map((node) => {
                const entry: FloorPlanGridNode = { nodeType: node.nodeType };
                if (node.nodeProps) {
                  entry.nodeProps = node.nodeProps;
                }
                return entry;
              })
            ),
          },
        ]);
        openAlert("Saved!", AlertType.Info);
        setLoadingGrid(false);
      } catch (error) {
        setLoadingGrid(false);
        if (error.message) {
          openAlert(error.message, AlertType.Error);
        } else {
          openAlert("Something went wrong");
        }
      }
    };
    return (
      <FloorPlanNavBar title={businessTitle} onSave={handleFloorPlanSave} />
    );
  }, [businessId, businessTitle, currentFloor, gridNodes, openAlert]);

  const toolbarView = useMemo(() => {
    const handleSelectTool = (tool: ToolbarToolType) => {
      if (disableSelector.has(tool)) return;
      switch (selectedTool) {
        case ToolbarToolType.Line:
          deselectLineTool();
          break;
        case ToolbarToolType.Rectangle:
          deselectRectangleTool();
          break;
        case ToolbarToolType.Waypoint:
          break;
        default:
      }
      setSelectedTool(tool);
    };
    return (
      <FloorPlanToolBar
        selectedTool={selectedTool}
        onToolSelected={handleSelectTool}
      />
    );
  }, [deselectLineTool, deselectRectangleTool, disableSelector, selectedTool]);

  const alertView = useMemo(
    () => (
      <Alert
        isOpen={isAlertOpen}
        type={alertType}
        closeAlert={closeAlert}
        autohide
      >
        {alertMessage}
      </Alert>
    ),
    [alertMessage, alertType, closeAlert, isAlertOpen]
  );

  const floorGirdNodeView = useMemo(() => {
    const nodeSelected = (row: number, col: number) => {
      if (gridNodes[row][col].nodeType === FloorGridNodeType.Waypoint) {
        // waypointNodeSelected(row, col);
        openWaypointPrompt(row, col, gridNodes[row][col].nodeProps?.title);
        return;
      }
      switch (selectedTool) {
        case ToolbarToolType.Line:
          lineNodeSelected(row, col);
          break;
        case ToolbarToolType.Rectangle:
          rectangleNodeSelect(row, col);
          break;
        case ToolbarToolType.Waypoint:
          openWaypointPrompt(row, col);
          break;
        default:
      }
    };

    return (
      <FloorPlanGrid onNodeSelected={nodeSelected} gridNodes={gridNodes} />
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [gridNodes, selectedTool]);

  const waypointModalView = useMemo(() => {
    return (
      <PromptWaypointOptions
        display={displayPrompt}
        onCancel={onCancelCreateWaypoint}
        currentTitle={waypointPromptCurrentTitle}
        onSave={onSaveWaypoint}
        onDelete={onDeleteWaypointCurrentWaypoint}
      />
    );
  }, [
    displayPrompt,
    onCancelCreateWaypoint,
    onDeleteWaypointCurrentWaypoint,
    onSaveWaypoint,
    waypointPromptCurrentTitle,
  ]);

  const gridLoadingView = useMemo(
    () => (loadingGrid ? <LinearProgress /> : <></>),
    [loadingGrid]
  );

  // ========== Render ==========
  return (
    <div className={classes.wrapper}>
      <div className={classes.header}>
        {waypointModalView}
        {alertView}
        {navbarView}
        {toolbarView}
        {gridLoadingView}
      </div>
      <div className={classes.content}>{floorGirdNodeView}</div>
    </div>
  );
};

export default FloorPlanEditPage;

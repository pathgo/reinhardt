import { makeStyles } from "@material-ui/core";
import React, { useEffect } from "react";
// import CallMade from "@material-ui/icons/CallMade";
import CheckBoxOutlineBlankIcon from "@material-ui/icons/CheckBoxOutlineBlank";
import AddLocationIcon from "@material-ui/icons/AddLocation";
// import NearMeIcon from "@material-ui/icons/NearMe";
import Button from "../../../components/button";
import Text from "../../../components/Text";
import { ThemeColor } from "../../../constant/theme";

export enum ToolbarToolType {
  Pointer,
  Line,
  Rectangle,
  UserLocation,
  Waypoint,
}

export type FloorPlanToolBarProps = {
  onToolSelected: (tool: ToolbarToolType) => void;
  selectedTool: ToolbarToolType;
};

const useStyles = makeStyles({
  wrapper: {
    backgroundColor: ThemeColor.Dark,
    display: "flex",
    justifyContent: "left",
  },
  inner: {
    padding: "5px 10px 5px 5px",
  },
  selectedTool: {
    opacity: 0.5,
  },
});

const FloorPlanToolBar: React.FC<FloorPlanToolBarProps> = ({
  onToolSelected,
  selectedTool,
}) => {
  const classes = useStyles();
  const toolbarClasses = (type: ToolbarToolType) =>
    type === selectedTool ? classes.selectedTool : "";

  useEffect(() => {
    // TODO: Enable Keyboard Listener for selecting tool
    // Handle (Shift + Key) press
    // const handleKeyPress = (e: KeyboardEvent) => {
    //   switch (e.key) {
    //     case "l":
    //       onToolSelected(ToolbarToolType.Line);
    //       break;
    //     case "r":
    //       onToolSelected(ToolbarToolType.Rectangle);
    //       break;
    //     case "p":
    //       onToolSelected(ToolbarToolType.Pointer);
    //       break;
    //     case "w":
    //       onToolSelected(ToolbarToolType.Waypoint);
    //       break;
    //     default:
    //   }
    // };
    // document.addEventListener("keydown", handleKeyPress);
    // return () => document.removeEventListener("keydown", handleKeyPress);
  }, [onToolSelected]);

  return (
    <div className={classes.wrapper}>
      <div className={classes.inner}>
        {/* <Button
          className={toolbarClasses(ToolbarToolType.Pointer)}
          onClick={() => onToolSelected(ToolbarToolType.Pointer)}
        >
          <CallMade />
        </Button> */}
        <Button
          className={toolbarClasses(ToolbarToolType.Line)}
          onClick={() => onToolSelected(ToolbarToolType.Line)}
        >
          <Text color={ThemeColor.White}>/</Text>
        </Button>
        <Button
          className={toolbarClasses(ToolbarToolType.Rectangle)}
          onClick={() => onToolSelected(ToolbarToolType.Rectangle)}
        >
          <CheckBoxOutlineBlankIcon />
        </Button>
      </div>
      <div className={classes.inner}>
        {/* <Button
          className={toolbarClasses(ToolbarToolType.UserLocation)}
          onClick={() => onToolSelected(ToolbarToolType.UserLocation)}
        >
          <NearMeIcon />
        </Button> */}
        <Button
          className={toolbarClasses(ToolbarToolType.Waypoint)}
          onClick={() => onToolSelected(ToolbarToolType.Waypoint)}
        >
          <AddLocationIcon />
        </Button>
      </div>
    </div>
  );
};

export default FloorPlanToolBar;

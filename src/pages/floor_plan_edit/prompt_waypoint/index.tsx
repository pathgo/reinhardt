import {
  Card,
  CardActions,
  CardContent,
  makeStyles,
  Modal,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import Button from "../../../components/button";
import TextFieldComponent from "../../../components/form/textfield";
import Text from "../../../components/Text";
import { ThemeColor } from "../../../constant/theme";

export type PromptWaypointOptionsProps = {
  display: boolean;
  currentTitle?: string;
  onSave: (newTitle: string) => void;
  onDelete: () => void;
  onCancel: () => void;
};

const useStyles = makeStyles({
  root: {
    height: "100vh",
    zIndex: 99,
    marginLeft: "auto",
    marginRight: "auto",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  content: {
    width: "30vw",
  },
});

export const PromptWaypointOptions: React.FC<PromptWaypointOptionsProps> = ({
  display,
  currentTitle,
  onSave,
  onCancel,
  onDelete,
}) => {
  const [title, setTitle] = useState("");
  const [errorMessage, setErrorMessage] = useState<string>();

  const classes = useStyles();

  useEffect(() => {
    if (currentTitle) {
      setTitle(currentTitle);
    } else {
      setTitle("");
    }
  }, [currentTitle]);

  const handleSave = () => {
    setErrorMessage(undefined);
    try {
      onSave(title);
      setTitle("");
    } catch (error) {
      setErrorMessage(error.message || "Something went wrong");
    }
  };

  const handleDelete = () => {
    setErrorMessage(undefined);
    onDelete();
  };

  const handleCancel = () => {
    setErrorMessage(undefined);
    onCancel();
  };

  // TODO: Refactor modal to component folder
  return (
    <Modal open={display}>
      <div className={classes.root}>
        <Card>
          <CardContent className={classes.content}>
            <div>
              {errorMessage && (
                <Text color={ThemeColor.Danger}>{errorMessage}</Text>
              )}
              <TextFieldComponent
                placeholder="Waypoint Title"
                value={title}
                fullWidth
                onChange={(e) => {
                  setTitle(e.target.value);
                }}
              />
            </div>
          </CardContent>
          <CardActions>
            <Button onClick={handleSave} color={ThemeColor.Success}>
              Save
            </Button>
            <Button onClick={handleDelete} color={ThemeColor.Danger}>
              Delete
            </Button>
            <div style={{ flexGrow: 1 }} />
            <Button onClick={handleCancel} color={ThemeColor.Light}>
              Cancel
            </Button>
          </CardActions>
        </Card>
      </div>
    </Modal>
  );
};

import {
  ButtonBase,
  Grid,
  makeStyles,
  Paper,
  Typography,
} from "@material-ui/core";
import React from "react";
import { ThemeColor } from "../../../constant/theme";

export type SearchListItemProps = {
  businessName: string;
  address: string;
  onClick: () => void;
};

const useStyles = makeStyles({
  listItem: {
    boxSizing: "border-box",
    listStyleType: "none",
    border: "2px solid #3D5A80",
    borderRadius: "10px",
    backgroundColor: "#98C1D9",
    marginBottom: "18px",
    padding: "5px",
    "&:hover": {
      cursor: "pointer",
    },
  },
  businessTitle: {
    color: ThemeColor.Primary,
    fontSize: 24,
    fontWeight: "bold",
    paddingBottom: "12px",
  },
  address: {
    color: ThemeColor.Black,
    fontWeight: "lighter",
    opacity: 0.85,
  },
  root: {
    flexGrow: 1,
    paddingTop: "10px",
  },

  paper: {
    backgroundColor: ThemeColor.Light,
    "&:hover": {
      cursor: "pointer",
    },
    padding: "10px",
  },
  image: {
    width: 128,
    height: 128,
  },
  img: {
    margin: "auto",
    display: "block",
    maxWidth: "100%",
    maxHeight: "100%",
  },
});

const FloorPlanSearchListItem: React.FC<SearchListItemProps> = ({
  businessName,
  address,
  onClick,
}) => {
  const classes = useStyles();
  return (
    <div className={classes.root} onClick={onClick}>
      <Paper className={classes.paper}>
        <Grid container spacing={2}>
          <Grid item>
            <ButtonBase className={classes.image}>
              <img
                className={classes.img}
                alt="QR Code or location"
                src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Target_Corporation_logo_%28vector%29.svg/1200px-Target_Corporation_logo_%28vector%29.svg.png"
              />
            </ButtonBase>
          </Grid>
          <Grid item xs={12} sm container>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs>
                <Typography
                  className={classes.businessTitle}
                  gutterBottom
                  variant="subtitle1"
                >
                  {businessName}
                </Typography>
                <Typography
                  className={classes.address}
                  variant="body2"
                  gutterBottom
                >
                  {address}
                </Typography>
              </Grid>
              <Grid item>
                <Typography variant="body2" style={{ cursor: "pointer" }}>
                  Remove
                </Typography>
              </Grid>
            </Grid>
            <Grid item>
              <Typography variant="subtitle1">5 Miles</Typography>
            </Grid>
          </Grid>
        </Grid>
      </Paper>
    </div>
  );
};

export default FloorPlanSearchListItem;

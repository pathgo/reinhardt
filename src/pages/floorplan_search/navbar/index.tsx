import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextFieldComponent from "../../../components/form/textfield";
import NavBar from "../../../components/navbar";
import useFormChange from "../../../components/form/textfield/useFormChange";
import { ThemeColor } from "../../../constant/theme";

export type SearchBarProps = {
  onSearch: (title: string) => void;
  initialTitle?: string;
};

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
  },
});

const SearchBar: React.FC<SearchBarProps> = ({
  onSearch,
  initialTitle = "",
}) => {
  const classes = useStyles();
  const [businessName, setbusinessName] = useFormChange(initialTitle);
  useEffect(() => {
    const handleEnterKey = (e: KeyboardEvent) => {
      if (e.key === "Enter") {
        onSearch(businessName);
      }
    };
    document.addEventListener("keydown", handleEnterKey);

    return () => {
      document.removeEventListener("keydown", handleEnterKey);
    };
  }, [businessName, onSearch]);
  return (
    <div className={classes.root}>
      <NavBar
        color={ThemeColor.Primary}
        right={
          <>
            <TextFieldComponent
              type="search"
              label="Search"
              value={businessName}
              onChange={setbusinessName}
            />
          </>
        }
      />
    </div>
  );
};

export default SearchBar;

import React, { useEffect, useState } from "react";
import { createStyles, makeStyles } from "@material-ui/core/styles";
import { useHistory } from "react-router-dom";
import { Container } from "@material-ui/core";
import SearchBar from "./navbar/index";
import { ThemeColor } from "../../constant/theme";
import { BusinessModel } from "../../service/api/types";
import BusinessService from "../../service/api/business";
import LinearProgress from "../../components/linear_progress";
import Alert, { AlertType, useAlertState } from "../../components/alert";
import useQueryParams from "../../utils/router/use_query_params";
import BusinessItemList from "../../components/business_list_item/BusinessItemList";

const useStyles = makeStyles((theme: ThemeColor) =>
  createStyles({
    root: {
      flexGrow: 1,
      backgroundColor: theme,
    },
    header: {
      textAlign: "center",
    },
  })
);

const FloorPlanSearchPage: React.FC = () => {
  const classes = useStyles();
  const { title: querySearchTitle } = useQueryParams();
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [searchResults, setSearchResults] = useState<Array<BusinessModel>>([]);
  const {
    isOpen: alertOpen,
    openAlert,
    closeAlert,
    alertMessage,
    alertType,
  } = useAlertState();

  useEffect(() => {
    const handleSearchTitle = async (title: string) => {
      if (title.length) {
        try {
          setLoading(true);
          const result = await BusinessService.FindByTitle(title);
          setSearchResults(result);
          if (result.length === 0) {
            openAlert("No Businesses Found", AlertType.Error);
          }
          setLoading(false);
        } catch (error) {
          openAlert(error.message || "Something Went Wrong", AlertType.Error);
          setLoading(false);
        }
      }
    };

    if (querySearchTitle?.length) handleSearchTitle(querySearchTitle);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [querySearchTitle]);

  const onSearch = (title: string) => {
    if (title.length) {
      history.push(`/floorplan/search?title=${title}`);
    }
  };

  return (
    <div className={classes.root}>
      <Alert
        autohide
        isOpen={alertOpen}
        type={alertType}
        closeAlert={closeAlert}
      >
        {alertMessage}
      </Alert>
      <SearchBar initialTitle={querySearchTitle} onSearch={onSearch} />
      {loading && <LinearProgress />}
      {/* <FloorPlanSearchItemList results={searchResults} /> */}
      <Container>
        <BusinessItemList results={searchResults} />
      </Container>
    </div>
  );
};

export default FloorPlanSearchPage;

import { makeStyles } from "@material-ui/core";
import React from "react";
import Hero from "./hero";
import RecommendedPlaces from "./recommended_businesses";
import Footer from "./footer";
import SimpleNavbarLayout from "../../components/layout/simple/navbar";

const useHomepageStyles = makeStyles({
  header: {
    textAlign: "center",
  },
  content: {
    maxWidth: "100%",
    display: "flex",
    flexDirection: "column",
    flex: "1 0 auto",
  },
  footer: {
    flexShrink: 0,
  },
  navlinkButton: {
    display: "block",
    marginBlock: "20px",
  },
});

const HomePage: React.FC = () => {
  const classes = useHomepageStyles();

  return (
    <SimpleNavbarLayout>
      <>
        <div className={classes.content}>
          <div className={classes.header}>
            <Hero />
          </div>
          <RecommendedPlaces />
        </div>
        <div className={classes.footer}>
          <Footer />
        </div>
      </>
    </SimpleNavbarLayout>
  );
};

export default HomePage;

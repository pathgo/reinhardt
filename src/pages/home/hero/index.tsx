import { makeStyles } from "@material-ui/core";
import React from "react";
import { useHistory } from "react-router-dom";
import Alert, { useAlertState } from "../../../components/alert";
import HeroSearch from "./search";
import Logo from "../../../assets/images/logo/PathGoLogoOutline.png";
import { ThemeColor } from "../../../constant/theme";
import BackgroundImage from "../../../assets/images/bg_home.jpg";

const useStyles = makeStyles({
  wrapper: {
    maxWidth: "100%",
  },
  heroUserImage: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.4)), url(${BackgroundImage})`,
    height: "50vh",
    maxWidth: "100%",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    // position: "relative",
    color: "white",
  },
  heroText: {
    fontSize: "2.5vh",
    color: ThemeColor.Light,
    fontWeight: 300,
  },

  logo: {
    width: "30vw",
    minWidth: "300px",
  },
});

const Hero: React.FC = () => {
  const classes = useStyles();
  const history = useHistory();
  const {
    isOpen: alertOpen,
    closeAlert,
    alertMessage,
    alertType,
  } = useAlertState();

  const onSearch = (title: string) => {
    if (title.length) {
      history.push(`/floorplan/search?title=${title}`);
    }
  };
  return (
    <div>
      <div className={classes.heroUserImage}>
        <div>
          <Alert
            autohide
            isOpen={alertOpen}
            type={alertType}
            closeAlert={closeAlert}
          >
            {alertMessage}
          </Alert>
          <img className={classes.logo} src={Logo} alt="Website Logo" />
          <h1 className={classes.heroText}>Find Your Way</h1>
          <br />
          <HeroSearch onSearch={onSearch} />
          <br />
          <br />
        </div>
      </div>
    </div>
  );
};
export default Hero;

import { Chip, CircularProgress } from "@material-ui/core";
import React, { useEffect, useMemo, useState } from "react";
import Text from "../../components/Text";
import APIRequest from "../../service/api/request";

const APIStatus: React.FC = () => {
  const [connected, setConnected] = useState<undefined | boolean>();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const checkConnection = async () => {
      try {
        setLoading(true);
        await APIRequest.get("echo");
        setConnected(true);
      } catch (error) {
        setConnected(false);
      } finally {
        setLoading(false);
      }
    };

    checkConnection();
  }, []);

  const statusView = useMemo(() => {
    if (loading || connected === undefined) return <CircularProgress />;
    const label = connected ? "Connected!" : "Unable to Connect";
    return (
      <Chip variant="outlined" color="secondary" size="small" label={label} />
    );
  }, [connected, loading]);

  return <Text>API Status: {statusView}</Text>;
};

export default APIStatus;

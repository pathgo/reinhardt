import { Divider, makeStyles, Paper } from "@material-ui/core";
import React from "react";
import { List as LoadingList } from "react-content-loader";
import BusinessItemList from "../../../components/business_list_item/BusinessItemList";
import { ThemeColor } from "../../../constant/theme";
import { BusinessModelSummary } from "../../../service/api/types";

export type RecommendedCategoryProps = {
  title: string;
  icon: React.ElementType;
  businesses: BusinessModelSummary[];
  loading?: boolean;
};

const useStyles = makeStyles({
  paper: {
    padding: "18px",
    textAlign: "center",
    color: ThemeColor.Secondary,
    minHeight: "20vh",
  },
});

const RecommendedCategory: React.FC<RecommendedCategoryProps> = ({
  title,
  icon: Icon,
  businesses,
  loading,
}) => {
  const classes = useStyles();
  return (
    <Paper className={classes.paper}>
      <Icon />
      <br />
      <br />
      <Divider />
      <h3>{title}</h3>
      {loading ? <LoadingList /> : <BusinessItemList results={businesses} />}
    </Paper>
  );
};

export default RecommendedCategory;

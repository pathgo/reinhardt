import React, { useEffect, useState } from "react";
import WhatshotIcon from "@material-ui/icons/Whatshot";
import BusinessService from "../../../service/api/business";
import { BusinessModelSummary } from "../../../service/api/types";
import RecommendedCategory from "./recommended_category";
import { useErrorAlert } from "../../../hooks/alert";

const PopularBusinessComponent: React.FC = () => {
  const [popularBusinesses, setPopularBusinesses] = useState<
    Array<BusinessModelSummary>
  >([]);
  const [loading, setLoading] = useState(false);
  const showError = useErrorAlert();

  useEffect(() => {
    const handleCookieRead = async () => {
      setLoading(true);
      try {
        const businesses = await BusinessService.GetPopularBusinesses();
        setPopularBusinesses(businesses);
      } catch (error) {
        showError(error.message || "Unable to fetch popular businesses");
      } finally {
        setLoading(false);
      }
    };
    handleCookieRead();
  }, []);

  return (
    <RecommendedCategory
      title="Popular"
      loading={loading}
      businesses={popularBusinesses}
      icon={WhatshotIcon}
    />
  );
};

export default PopularBusinessComponent;

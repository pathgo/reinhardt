import React, { useEffect, useState } from "react";
import NearMeIcon from "@material-ui/icons/NearMe";
import BusinessService from "../../../service/api/business";
import { BusinessModelSummary } from "../../../service/api/types";
import RecommendedCategory from "./recommended_category";
import Geocode from "../../../service/geocode";
import { useErrorAlert } from "../../../hooks/alert";

const NearMeBusinessComponent: React.FC = () => {
  const [businessNearMe, setBusinessNearMe] = useState<
    Array<BusinessModelSummary>
  >([]);
  const [loading, setLoading] = useState(false);
  const showError = useErrorAlert();

  useEffect(() => {
    setLoading(true);
    navigator.geolocation.getCurrentPosition(async (position) => {
      const { latitude, longitude } = position.coords;
      try {
        const zip = await Geocode.GetZipCode(latitude, longitude);
        if (zip) {
          const businesses = await BusinessService.GetByZipcode(zip);
          if (businesses.length > 5) {
            setBusinessNearMe(businesses.slice(0, 5));
          } else {
            setBusinessNearMe(businesses);
          }
        }
      } catch (error) {
        showError(error.message || "Unable to fetch businesses near you");
      } finally {
        setLoading(false);
      }
    });
  }, []);

  return (
    <RecommendedCategory
      title="Near Me"
      loading={loading}
      businesses={businessNearMe}
      icon={NearMeIcon}
    />
  );
};

export default NearMeBusinessComponent;

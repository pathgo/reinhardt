import { makeStyles } from "@material-ui/core";
import React from "react";
import { ThemeColor } from "../../../constant/theme";

const useStyles = makeStyles({
  wrapper: {
    display: "flex",
    flexDirection: "column",
    maxWidth: "100%",
    backgroundColor: ThemeColor.Primary,
    color: "white",
    paddingTop: "10px",
    paddingBottom: "10px",
  },

  column: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-evenly",
  },

  heroText: {
    textAlign: "center",
    margin: "0%",
  },
});

const Footer: React.FC = () => {
  const classes = useStyles();
  return (
    <div className={classes.wrapper}>
      <div>
        <p className={classes.heroText}>
          PathGo &copy; {new Date().getFullYear()}
        </p>
      </div>
    </div>
  );
};

export default Footer;

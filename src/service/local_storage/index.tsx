const RECENTLY_VISITED_KEY = "POPULAR_BUSINESSES_V2";
const GEOCODED_ZIP_KEY = "GEOCODED_ZIP_KEY";

const GeocodeStorageKey = (latitude: number, longitude: number): string =>
  `${latitude}-${longitude}`;

export default class AppStorage {
  static AddToRecentlyVisited(businessId: string): void {
    const BusinessString = localStorage.getItem(RECENTLY_VISITED_KEY);
    if (BusinessString === null) {
      localStorage.setItem(RECENTLY_VISITED_KEY, businessId);
    } else {
      const existingBusinesses = BusinessString.split(";");
      if (existingBusinesses.findIndex((id) => businessId === id) !== -1) {
        // Already in recently visited
        return;
      }
      if (existingBusinesses.length >= 3) {
        existingBusinesses.pop();
      }
      existingBusinesses.unshift(businessId);
      const idstring = existingBusinesses.join(";");
      localStorage.setItem(RECENTLY_VISITED_KEY, idstring);
    }
  }

  static RemoveFromRecentIfExists(businessId: string): void {
    const BusinessString = localStorage.getItem(RECENTLY_VISITED_KEY);
    if (BusinessString === null) {
      return;
    }
    const existingBusinesses = BusinessString.split(";");
    const indexToDelete = existingBusinesses.findIndex(
      (id) => id === businessId
    );
    if (indexToDelete === -1) return;
    existingBusinesses.splice(indexToDelete, 1);
    const idstring = existingBusinesses.join(";");
    localStorage.setItem(RECENTLY_VISITED_KEY, idstring);
  }

  static RecentlyVisited(): Array<string> {
    const allBusinessesIDString = localStorage.getItem(RECENTLY_VISITED_KEY);
    if (allBusinessesIDString === null || allBusinessesIDString === undefined)
      return [];
    return allBusinessesIDString.split(";");
  }

  static SaveZipcode(
    latitude: number,
    longitude: number,
    zipcode: number
  ): void {
    const items = localStorage.getItem(GEOCODED_ZIP_KEY);
    let geocodeToZipcode: Record<string, string> = {};
    if (items) {
      const existingItems = JSON.parse(items);
      geocodeToZipcode = {
        ...existingItems,
      };
    }
    geocodeToZipcode[GeocodeStorageKey(latitude, longitude)] = `${zipcode}`;
    localStorage.setItem(GEOCODED_ZIP_KEY, JSON.stringify(geocodeToZipcode));
  }

  static GetZipcode(latitude: number, longitude: number): number | undefined {
    const items = localStorage.getItem(GEOCODED_ZIP_KEY);
    if (items === undefined || items === null) return undefined;
    const parsedItem = JSON.parse(items);
    const zipcode = parsedItem[GeocodeStorageKey(latitude, longitude)];
    if (zipcode) return parseInt(zipcode, 10);
    return undefined;
  }
}

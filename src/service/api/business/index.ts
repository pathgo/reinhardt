import floorplan from "../../../config/floorplan";
import AuthService from "../../auth";
import APIRequest from "../request";
import {
  BusinessModel,
  BusinessModelSummary,
  CreateBusinessProps,
} from "../types";

type FindByTitleResponse = {
  businessList: Array<BusinessModel>;
};

type GetUserBusinessResponse = {
  businesses: Array<BusinessModelSummary>;
};

type SearchBusinessResource = {
  businesses: Array<BusinessModelSummary>;
};

export default class BusinessService {
  static async Create(props: CreateBusinessProps): Promise<BusinessModel> {
    const ownerId = await AuthService.CurrentUserId();
    const body = {
      ...props,
      ownerId,
      floorplanConfig: {
        rowCount: floorplan.initialRowCount,
        columnCount: floorplan.initialColumnCount,
      },
    };
    const response = await APIRequest.post<BusinessModel>("business", body);
    return response;
  }

  static async FindByTitle(title: string): Promise<Array<BusinessModel>> {
    const url = `business/search?title=${encodeURIComponent(title)}`;
    const response = await APIRequest.get<FindByTitleResponse>(url);
    return response.businessList;
  }

  static async FindById(id: string): Promise<BusinessModel> {
    const url = `business/search?id=${encodeURIComponent(id)}`;
    const response = await APIRequest.get<BusinessModel>(url);
    return response;
  }

  static async GetCurrentUserBusinesses(): Promise<
    Array<BusinessModelSummary>
  > {
    const userId = await AuthService.CurrentUserId();
    const url = `user/id/${userId}/businesses`;
    const response = await APIRequest.get<GetUserBusinessResponse>(url);
    return response.businesses;
  }

  static async GetPopularBusinesses(): Promise<Array<BusinessModelSummary>> {
    const response = await APIRequest.get<SearchBusinessResource>(
      "business/analytics/hits/top"
    );
    return response.businesses;
  }

  static async GetByZipcode(zipcode: number): Promise<BusinessModelSummary[]> {
    const response = await APIRequest.get<SearchBusinessResource>(
      `business/zip/getbyzip?zip=${zipcode}`
    );
    return response.businesses;
  }

  static async DeleteBusiness(businessId: string): Promise<void> {
    await APIRequest.delete(`business/id/${businessId}`);
  }
}

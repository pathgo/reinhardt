export enum FloorGridNodeType {
  Wall = "Wall",
  Path = "Path",
  Waypoint = "Waypoint",
}
export type LocationGridNodeProps = {
  row: number;
  col: number;
};

export type WaypointGridNodeProps = {
  title: string;
};

export type FloorPlanGridNodeProps = WaypointGridNodeProps;

export type FloorPlanGridNode = {
  nodeType: FloorGridNodeType;
  nodeProps?: FloorPlanGridNodeProps;
};

export type FloorPlanArrayIndex = {
  floorTitle: string;
  floorGrid: Array<Array<FloorPlanGridNode>>;
};

export type FloorPlanModel = Array<FloorPlanArrayIndex>;

export type BusinessAddress = {
  streetAddress: string;
  unit?: string;
  state: string;
  city: string;
  zipcode: number;
};

export type CreateBusinessProps = {
  businessName: string;
  address: BusinessAddress;
};

export interface BusinessModel extends CreateBusinessProps {
  id: string;
  floorPlan: FloorPlanModel;
  imageUrl?: string;
}

export type BusinessModelSummary = Omit<BusinessModel, "floorPlan">;

export interface WaypointSummary {
  title: string;
  row: number;
  column: number;
}

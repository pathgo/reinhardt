import { LocationGridNodeProps } from "../../service/api/types";

export default class NavigationPathList {
  private hashedSet: Set<string>;

  constructor(nodes: Array<LocationGridNodeProps>) {
    this.hashedSet = new Set();
    nodes.forEach(({ row, col }) => {
      this.hashedSet.add(NavigationPathList.GetKey(row, col));
    });
  }

  has = (row: number, col: number): boolean =>
    this.hashedSet.has(NavigationPathList.GetKey(row, col));

  private static GetKey(row: number, col: number) {
    return `${row}-${col}`;
  }
}

import React from "react";
import { render } from "@testing-library/react";
import Text from ".";

describe("Test components/Text", () => {
  it("Renders child text", () => {
    const expectedTxt = "Hello World";
    const { getByText } = render(<Text>{expectedTxt}</Text>);
    expect(getByText(expectedTxt)).toBeTruthy();
  });
});

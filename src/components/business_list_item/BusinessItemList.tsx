import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useHistory } from "react-router-dom";
import { BusinessModelSummary } from "../../service/api/types";
import BusinessListItem from "./BusinessListItem";

const useStyles = makeStyles({
  optionWrapper: {
    display: "flex",
    justifyContent: "flex-end",
  },
  root: {
    marginTop: "24px",
    flexGrow: 1,
    // marginLeft: "50px",
    // marginRight: "50px",
  },
});

interface ResultProps {
  results: Array<BusinessModelSummary>;
}

const BusinessItemList: React.FC<ResultProps> = ({ results }) => {
  const classes = useStyles();
  const history = useHistory();

  return (
    <div className={classes.root}>
      <div>
        {results.map((item, index) => (
          <BusinessListItem
            // eslint-disable-next-line react/no-array-index-key
            key={index}
            businessName={item.businessName}
            address={item.address.streetAddress}
            zipcode={item.address.zipcode}
            id={item.id}
            onClick={() => history.push(`/pathfinder/${item.id}/prompt`)}
          />
        ))}
      </div>
    </div>
  );
};

export default BusinessItemList;

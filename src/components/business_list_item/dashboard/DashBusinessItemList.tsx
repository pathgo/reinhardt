import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { BusinessModelSummary } from "../../../service/api/types";
import DashBusinessListItem from "./index";

const useStyles = makeStyles({
  optionWrapper: {
    display: "flex",
    justifyContent: "flex-end",
  },
  root: {
    marginTop: "24px",
    flexGrow: 1,
    marginLeft: "50px",
    marginRight: "50px",
  },
});

interface ResultProps {
  results: Array<BusinessModelSummary>;
  onDelete: (businessId: string) => void;
}

const DashBusinessItemList: React.FC<ResultProps> = ({ results, onDelete }) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div>
        {results.map((item) => (
          <DashBusinessListItem
            key={item.id}
            businessName={item.businessName}
            address={item.address.streetAddress}
            zipcode={item.address.zipcode}
            id={item.id}
            onDelete={() => onDelete(item.id)}
          />
        ))}
      </div>
    </div>
  );
};

export default DashBusinessItemList;

import { makeStyles } from "@material-ui/core";
import { ThemeColor } from "../../constant/theme";

export default makeStyles({
  whiteText: {
    color: ThemeColor.White,
  },
  navlink: {
    textDecoration: "none",
  },
});

import React from "react";
import { TextField } from "@material-ui/core";
import { fireEvent, render } from "@testing-library/react";
import { act } from "react-dom/test-utils";

describe("Test Text Field Component", () => {
  it("Renders label", () => {
    const expectedLabel = "Example label";
    const { getByText } = render(<TextField label={expectedLabel} />);
    expect(getByText(expectedLabel)).toBeTruthy();
  });

  it("Renders helper text", () => {
    const expectedText = "Example Helper Text";
    const { getByText } = render(<TextField helperText={expectedText} />);
    expect(getByText(expectedText)).toBeTruthy();
  });

  it("Changes text on type for controlled input", () => {
    const expectedPlaceholder = "Example Placeholder";
    const inputTargetValue = "Example Txt";
    const { getByPlaceholderText } = render(
      <TextField placeholder={expectedPlaceholder} />
    );
    const input = getByPlaceholderText(expectedPlaceholder) as HTMLInputElement;
    act(() => {
      fireEvent.change(input, {
        target: {
          value: inputTargetValue,
        },
      });
    });
    expect(input.value).toEqual(inputTargetValue);
  });
});

import { makeStyles } from "@material-ui/core";
import React, { useState } from "react";
import { StandardReguarUserNavItems } from "../../../config/navbar";
import { ThemeColor } from "../../../constant/theme";
import Button from "../../button";
import NavBar from "../../navbar";
import Text from "../../Text";

export type AuthLayoutProps = {
  title: string;
  onSubmit: () => Promise<void>;
  buttonTitle?: string;
};

const useStyles = makeStyles({
  container: {
    marginTop: "72px",
    marginLeft: "auto",
    marginRight: "auto",
    maxWidth: "40vw",
  },
  header: {
    margin: 0,
    textAlign: "center",
  },
  formStyle: {
    paddingTop: "24px",
    paddingBottom: "24px",
    border: "3px solid #3D5A80",
    padding: "0 24px",
  },
  button: {
    marginTop: "24px",
    textAlign: "center",
  },
  error: {
    margin: "12px 0",
  },
});

const AuthLayout: React.FC<AuthLayoutProps> = ({
  title,
  children,
  buttonTitle = "Submit",
  onSubmit,
}) => {
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState<string>();

  const handleSubmit = async () => {
    try {
      setLoading(true);
      await onSubmit();
    } catch (error) {
      setErrorMessage(error.message || "Something went wrong");
    } finally {
      setLoading(false);
    }
  };

  return (
    <div>
      <NavBar navItems={StandardReguarUserNavItems} />
      <div className={classes.container}>
        <form className={classes.formStyle}>
          <h1 className={classes.header}>{title}</h1>
          {errorMessage && (
            <Text className={classes.error} color={ThemeColor.Danger}>
              {errorMessage}
            </Text>
          )}
          {children}
          <div className={classes.button}>
            <Button
              color={ThemeColor.Primary}
              disabled={loading}
              onClick={handleSubmit}
            >
              {buttonTitle}
            </Button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default AuthLayout;

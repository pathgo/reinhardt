import { makeStyles } from "@material-ui/core";
import React from "react";
import cx from "classnames";
import { ThemeColor } from "../../constant/theme";

export type ToolTipProps = {
  title: string;
  alwaysOpen?: boolean;
};

const useStyles = makeStyles({
  tooltip: {
    position: "relative",
    display: "inline-block",
  },
  tooltipText: {
    visibility: "hidden",
    width: "120px",
    backgroundColor: ThemeColor.Dark,
    color: "#fff",
    textAlign: "center",
    borderRadius: "6px",
    padding: "5px 0",
    position: "absolute",
    zIndex: 1,
    bottom: "100%",
    left: "50%",
    marginLeft: "-60px",
    "&:hover": {
      visibility: "visible",
    },
  },
  alwaysOpenText: {
    visibility: "visible",
  },
});

const Tooltip: React.FC<ToolTipProps> = ({ alwaysOpen, title, children }) => {
  const classes = useStyles();
  const titleClass = cx({
    [classes.tooltipText]: true,
    [classes.alwaysOpenText]: alwaysOpen,
  });
  return (
    <div className={classes.tooltip}>
      <div className={titleClass}>{title}</div>
      {children}
    </div>
  );
};

export default Tooltip;

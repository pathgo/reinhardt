import React, { useEffect } from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import Popover from "@material-ui/core/Popover";
import Typography from "@material-ui/core/Typography";
import AddLocationIcon from "@material-ui/icons/AddLocation";
import { ThemeColor } from "../../constant/theme";
import TextFieldComponent from "../form/textfield";
import useFormChange from "../form/textfield/useFormChange";

export type WaypointPromptProps = {
  onEnter: (title: string) => void;
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    typography: {
      padding: theme.spacing(2),
    },
    node: {
      // outline: `1px solid ${ThemeColor.Dark}`,
      display: "inline-block",
      minWidth: "16px",
      maxWidth: "18px",
      minHeight: "16px",
      maxHeight: "18px",
      overflow: "hidden",
      "&:hover": {
        backgroundColor: ThemeColor.Light,
        opacity: 0.4,
      },
    },
  })
);

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export default function SimplePopover(props: WaypointPromptProps) {
  const [waypointName, setWaypointName] = useFormChange("Waypoint Name");
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(
    null
  );
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  useEffect(() => {
    const handleEnterKey = (e: KeyboardEvent) => {
      if (e.key === "Enter") {
        props.onEnter(waypointName);
        handleClose();
      }
    };
    document.addEventListener("keydown", handleEnterKey);

    return () => {
      document.removeEventListener("keydown", handleEnterKey);
    };
  }, [props, waypointName]);

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  return (
    <div>
      <button type="button" onClick={handleClick} className={classes.node}>
        <AddLocationIcon fontSize="small" />
      </button>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        <Typography className={classes.typography}>
          {/* <TextFieldComponent label={waypointName} onChange={setWaypointName} /> */}
          <TextFieldComponent
            type="name"
            label="Waypoint Name"
            value={waypointName}
            onChange={setWaypointName}
          />
        </Typography>
      </Popover>
    </div>
  );
}

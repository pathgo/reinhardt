import MLinearProgress from "@material-ui/core/LinearProgress";
import React from "react";

const LinearProgress: React.FC = () => (
  // eslint-disable-next-line react/jsx-props-no-spreading
  <MLinearProgress />
);

export default LinearProgress;

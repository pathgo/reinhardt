import { NavbarNavItem } from "../components/navbar";

export const StandardAuthUserNavItems: Array<NavbarNavItem> = [
  {
    title: "Dashboard",
    link: "/dashboard",
  },
  {
    title: "Logout",
    link: "/auth/logout",
  },
];

export const StandardReguarUserNavItems: Array<NavbarNavItem> = [
  {
    title: "Login",
    link: "/auth/login",
  },
  {
    title: "Signup",
    link: "/auth/signup",
  },
];

import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import BusinessDashboardPage from "../pages/business_dashboard";
import Error404Page from "../pages/error/error_404_page";
import FloorPlanSearchPage from "../pages/floorplan_search";
import FloorPlanCreatePage from "../pages/floor_plan_create";
import FloorPlanEditPage from "../pages/floor_plan_edit";
import HomePage from "../pages/home";
import PathFinderNavigationScreen from "../pages/path_finder_navigation";
import PathFinderPromptScreen from "../pages/path_finder_prompt";
import LoginPage from "../pages/login";
import SignUpPage from "../pages/signup";
import VerifyPage from "../pages/signup/verify";
import LogoutPage from "../pages/logout";
import DevPage from "../pages/dev";
import QRCodeGeneratorPage from "../pages/qr_code";

const AppRouter: React.FC = () => (
  <Switch>
    <Route exact path="/" component={HomePage} />
    <Route path="/auth/login" component={LoginPage} />
    <Route path="/auth/signup" component={SignUpPage} />
    <Route path="/auth/verify" component={VerifyPage} />
    <Route path="/auth/logout" component={LogoutPage} />
    <Route path="/floorplan/create" component={FloorPlanCreatePage} />
    <Route path="/floorplan/search" component={FloorPlanSearchPage} />
    <Route path="/floorplan/:businessId/edit" component={FloorPlanEditPage} />
    <Route
      path="/pathfinder/:businessId/prompt"
      component={PathFinderPromptScreen}
    />
    <Route
      path="/pathfinder/:businessId/navigate"
      component={PathFinderNavigationScreen}
    />
    <Route path="/dashboard" component={BusinessDashboardPage} />
    <Route path="/qr/generate" component={QRCodeGeneratorPage} />
    <Route path="/dev" component={DevPage} />
    <Route path="/error/404" component={Error404Page} />
    <Redirect to="/error/404" />
  </Switch>
);

export default AppRouter;

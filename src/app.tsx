import React, { useContext, useEffect } from "react";
import { AlertQueueContextProvider } from "./context/alert";
import UserContext from "./context/user";
import AppRouter from "./router";
import AuthService from "./service/auth";

const App: React.FC = () => {
  const { setUser } = useContext(UserContext);
  useEffect(() => {
    // TODO: Restore saved user
    const restoreUser = async () => {
      try {
        const user = await AuthService.RestoreSavedUser();
        setUser(user);
      } catch (error) {
        // Unauthenticated
      }
    };

    restoreUser();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <AlertQueueContextProvider>
      <AppRouter />
    </AlertQueueContextProvider>
  );
};

export default App;
